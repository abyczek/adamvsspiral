package ab

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks._

class SpiralRunnerSpec extends FreeSpec with Matchers {

  "SpiralRunner" - {

    val spiralRunner = new SpiralRunner {}

    val successfulTestCases = Table(
      ("location", "expected distance"),
      ("1", 0L),
      ("12", 3L),
      ("23", 2L),
      ("50", 7L),
      ("134", 11L),
      ("1024", 31L)
    )

    forAll(successfulTestCases) { (location, expectedDistance) =>
      s"should compute distance for a given valid location: $location" in {
        spiralRunner.computeManhattanDistance(Array(location)) shouldBe Right(expectedDistance)
      }
    }

    "should should compute distance for high location" in {
      val distanceForHighLocation = spiralRunner.computeManhattanDistance(Array("368078"))
      assert(distanceForHighLocation.isRight)
      println(s"Answer to task question. Distance for 368078 is ${distanceForHighLocation.right.get}")
    }

    "should return error if the input parameter (location) is not valid number" in {
      val invalidResult = spiralRunner.computeManhattanDistance(Array("this is not Long"))

      val wrong = invalidResult.left.get.asInstanceOf[WrongWithEx]
      wrong.errMsg shouldBe "Input argument could not be parsed to Location as Long type"
      wrong.throwable shouldBe a[java.lang.NumberFormatException]
    }

    "should return error if the input parameter (location) has invalid value" in {
      spiralRunner.computeManhattanDistance(Array("-1")) shouldBe Left(WrongWithMsg("Location value has to be greater than 0, while it is: -1"))
    }
  }
}
