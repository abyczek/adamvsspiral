package ab

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks._

class SpiralOpsSpec extends FreeSpec with Matchers {

  val RING5_MAX_LOC: Long = 121
  val RING6_MIN_LOC: Long = RING5_MAX_LOC + 1
  val RING6_MAX_LOC: Long = 169

  "SpiralOps" - {

    val spiralOps = new SpiralOps {
      override def ringSeekingMaxDepth: Int = 6
    }

    "findRing function" - {

      val findRingSuccessfulTestCases = Table(
        ("location", "expected ring", "description"),
        (1L, Ring(0, 0, 0), ", which is in the center of the spiral"),
        (82L, Ring(5, 40, RING5_MAX_LOC), ", which is the first location"),
        (118L, Ring(5, 40, RING5_MAX_LOC), ", which is the last location"),
        (121L, Ring(5, 40, RING5_MAX_LOC), ", which is a location in the middle"),

        (81L, Ring(4, 32, 81), ", which is the last location or previous Ring"),
        (RING6_MIN_LOC, Ring(6, 48, RING6_MAX_LOC), ", which is the first location of the next ring")
      )

      forAll(findRingSuccessfulTestCases) { (location, expectedRing, desc) =>
        s"should find the Ring for a given valid location: $location $desc" in {
          spiralOps.findRing(location) shouldBe Right(expectedRing)
        }
      }

      val ring7MinLocation = RING6_MAX_LOC + 1

      val findRingFailedTestCases = Table(
        ("location", "result", "description"),
        (0L, "Location value has to be greater than 0, while it is: 0", "it is less than 1"),
        (-1L, "Location value has to be greater than 0, while it is: -1", "it is less than 1"),
        (-100L, "Location value has to be greater than 0, while it is: -100", "it is less than 1"),
        (ring7MinLocation, s"Could not find a Ring for given Location: $ring7MinLocation", "seek max depth has been exceeded")
      )

      forAll(findRingFailedTestCases) { (location, errorMessage, desc) =>
        s"should return failure for location: $location because $desc" in {
          spiralOps.findRing(location) shouldBe Left(WrongWithMsg(errorMessage))
        }
      }

      "should find a ring even for high Locations" in {

        val spiralOpsWithMaxSeekDepth = new SpiralOps {}

        spiralOpsWithMaxSeekDepth.findRing(368078).isRight shouldBe true
      }

      "should find a 'ZERO' ring for center of the spiral" in {

        spiralOps.findRing(1) shouldBe Right(Ring(0, 0, 0))
      }
    }

    "calculateDistance" - {

      val ring = Ring(2, 16, 25)

      val calculateDistanceTestCases = Table(
        ("location", "expected distance", "description"),
        (13L, 4, "which is at 1st corner"),
        (17L, 4, "which is at 2nd corner"),
        (21L, 4, "which is at 3rd corner"),
        (25L, 4, "which is at 4th corner - max location"),

        (11L, 2, "which is in the middle of  1st window"),
        (15L, 2, "which is in the middle of  2nd window"),
        (19L, 2, "which is in the middle of  3rd window"),
        (23L, 2, "which is in the middle of  4th window"),

        (10L, 3, "which is is a minimal in the ring"),
        (12L, 3, "which is location after the center of the 1st window"),
        (14L, 3, "which is location before the center of the 2nd window"),
        (20L, 3, "which is location after the center of the 3rd window"),
        (22L, 3, "which is location before the center of the 4th window")
      )

      forAll(calculateDistanceTestCases) { (location, expectedDistance, desc) =>
        s"should return correct distance for the location: $location $desc" in {
          spiralOps.calculateDistance(ring, location) shouldBe Right(expectedDistance)
        }
      }

      "should calculate distance for the location at center of spiral over 'ZERO' ring" in {
        val zeroRing = Ring(0, 0, 0)
        spiralOps.calculateDistance(zeroRing, 1) shouldBe Right(0)
      }

      "should return error for ZERO Ring when location is different than 1" in {
        val zeroRing = Ring(0, 0, 0)
        spiralOps.calculateDistance(zeroRing, 3) shouldBe Left(WrongWithMsg("Location 3 does not fit to the ring: Ring(0,0,0)"))
      }
    }

    "validateRingForLocation" - {
      val ring = Ring(2, 16, 25)
      "should return valid Ring for location withing the Ring" in {
        SpiralOps.validateRingForLocation(ring, 24) shouldBe Right(ring)
      }

      "should return valid Ring for last location" in {
        SpiralOps.validateRingForLocation(ring, 25) shouldBe Right(ring)
      }
      "should return valid Ring for first location" in {
        SpiralOps.validateRingForLocation(ring, 10) shouldBe Right(ring)
      }

      "should return valid Ring for ZERO ring" in {
        SpiralOps.validateRingForLocation(Ring(0, 0, 0), 1) shouldBe Right(Ring(0, 0, 0))
      }

      "should return error message for ZERO Ring and location different than 1" in {
        SpiralOps.validateRingForLocation(Ring(0, 0, 0), 2) shouldBe Left(WrongWithMsg("Location 2 does not fit to the ring: Ring(0,0,0)"))
      }

      "should return error message for invalid Ring" in {
        val invalidRing = Ring(-1, 77, 111)
        SpiralOps.validateRingForLocation(invalidRing, 24) shouldBe
          Left(WrongWithMsg("Ring's number multiplied by 8 is not equal Ring's length, Rings length is not divisible by 4, Ring's last location should be divisible by 4"))
      }
    }
  }
}
