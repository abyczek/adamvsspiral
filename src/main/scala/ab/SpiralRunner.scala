package ab

import scala.util.Try

/**
  * Top level component providing functionality/api for calculating distance to the center of the spiral.
  * It does not have any dependency on hosting component - it should be easy to "inject" into any kind of application
  * like console app, actor or rest endpoint.
  */
trait SpiralRunner extends SpiralOps {

  def computeManhattanDistance(args: Array[String]): Result[Long] =
    for {
      inputParams <- getInputParameters(args)
      ring <- findRing(inputParams.location)
      manhattanDistance <- calculateDistance(ring, inputParams.location)
    } yield {
      manhattanDistance
    }

  protected def getInputParameters(args: Array[String]): Result[InputParams] = {
    Try(args(0).toLong)
      .map(InputParams)
      .toEither
      .left.map(ex => WrongWithEx("Input argument could not be parsed to Location as Long type", ex))
  }
}
