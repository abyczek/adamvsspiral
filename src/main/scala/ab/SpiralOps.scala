package ab

import cats.implicits._
import cats.data._
import cats.data.Validated._

trait SpiralOps {

  import SpiralOps._

  private val makeRing = (mkRing _).tupled

  /**
    * Parameter to restrict the depth of Ring seeking for given location.
    * [[SpiralOps]] will stop processing when number of searched rings reach the limit.
    */
  protected def ringSeekingMaxDepth: Int = Int.MaxValue

  def findRing(location: Long): Result[Ring] = {
    Either
      .cond(location > 0, location, WrongWithMsg(s"Location value has to be greater than 0, while it is: $location"))
      .flatMap(findRingForLocation)
  }

  def calculateDistance(ring: Ring, location: Long): Result[Long] =
    validateRingForLocation(ring, location).right.flatMap { r =>

      val firstLocation = r.lastLocation - r.length
      val windowSize = r.length / 4

      def findDistanceToQuarterMiddle(ringWindow: (Long, Long)): Result[Long] = {
        ringWindow match {
          case window@(_, windowMaxLoc) if windowMaxLoc > ring.lastLocation =>
            // this should never happen but it's better to report hypothetical error rather than leave infinite computation in the even of some bug
            Left(WrongWithMsg(s"FATAL. Computing distance within the Ring failed for the ring: $ring, window: $window and location: $location"))
          case (0L, 0L) =>
            Right(0L)
          case (windowLowerBound, windowMaxLoc) if location > windowLowerBound && location <= windowMaxLoc =>
            val windowMiddleLoc = windowLowerBound + (windowMaxLoc - windowLowerBound) / 2
            Right(Math.abs(location - windowMiddleLoc))
          case (_, windowMaxLoc) =>
            findDistanceToQuarterMiddle((windowMaxLoc, windowMaxLoc + windowSize))
        }
      }

      findDistanceToQuarterMiddle((firstLocation, firstLocation + windowSize))
        .map(_ + r.number)
    }

  private def findRingForLocation(location: Long): Either[Wrong, Ring] = location match {
    case ZERO_LOCATION => Right(ZERO_RING)
    case loc =>
      Stream
        .from(1)
        .takeWhile(_ <= ringSeekingMaxDepth)
        .map(computeRingLength)
        .scan(RING_ZERO_LENGTH)(computeRingLastLocation)
        .zipWithIndex
        .drop(1)
        .map(makeRing)
        .find(_.lastLocation >= loc)
        .toRight(WrongWithMsg(s"Could not find a Ring for given Location: $loc"))
  }

  private def computeRingLength(n: Int): Int = ADJACENT_RING_LENGTH_DELTA * n

  private def computeRingLastLocation(previousLastLocation: Int, ringLength: Int) = previousLastLocation + ringLength

  private def mkRing(ringLastLocation: Int, ringNumber: Int): Ring =
    Ring(ringNumber, ringNumber * ADJACENT_RING_LENGTH_DELTA, ringLastLocation)

}

object SpiralOps {

  val ZERO_LOCATION = 1
  val ZERO_RING = Ring(0, 0, 0)
  val ADJACENT_RING_LENGTH_DELTA = 8
  val RING_ZERO_LENGTH = 1

  def validateRingForLocation(ring: Ring, location: Long): Result[Ring] = {

    val validatedRing: Validated[NonEmptyList[String], Ring] =
      (validateRingNumber(ring).toValidatedNel,
        validateRingLength(ring).toValidatedNel,
        validateRingLastLocation(ring).toValidatedNel
      ).mapN(Ring)

    validatedRing
      .leftMap(x => x.reduceLeft(_ + ", " + _))
      .toEither.leftMap(WrongWithMsg(_))
      .flatMap(validateLocationForRing(location))
  }

  private def validateLocationForRing(location: Long)(ring: Ring): Result[Ring] = {
    Either.cond(
      (ring == ZERO_RING && location == 1)
        ||
        (ring.lastLocation >= location && (ring.lastLocation - ring.length) < location),
      ring,
      WrongWithMsg(s"Location $location does not fit to the ring: $ring")
    )
  }

  private def validateRingLastLocation(ring: Ring): Validated[String, Long] = {
    // IMPOROVEMENT - this is simplified validation but it could be based on math formula (similar to computing Ring itself for given location)
    Either.cond(ring.lastLocation == 0 || (ring.lastLocation - 1) % 4 == 0,
      ring.lastLocation,
      "Ring's last location should be divisible by 4"
    ).toValidated
  }

  private def validateRingLength(ring: Ring): Validated[String, Int] = {
    Either.cond(ring.length == 0 || ring.length % 4 == 0, ring.length, "Rings length is not divisible by 4").toValidated
  }

  private def validateRingNumber(ring: Ring): Validated[String, Int] = {
    Either.cond(
      ring.number == 0 || (ring.number * 8) == ring.length,
      ring.number,
      "Ring's number multiplied by 8 is not equal Ring's length").toValidated
  }
}
