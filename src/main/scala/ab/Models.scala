package ab

/**
  * Ring - represents the sequence of all locations round the inner-ring or the spiral center, which is 1.
  * For example: ring with number 1 would be [2,3,..8,9]
  *
  * There is a concept of ZERO ring with all parameters equal to 0 - and it represents center of the spiral - it is to cover edge case for distance = 0
  *
  * @param number       - sequential number of the ring, where 1 is the most inner ring.
  * @param length       - number of locations within the ring
  * @param lastLocation - max location in the Ring
  */
case class Ring(number: Int, length: Int, lastLocation: Long)

/**
  * Input parameters for "fun with spiral" job
  *
  * NOTE: it is assumed that grid size is not a input parameter, but if it was, this case class would be extended with one more param for it.
  *
  * @param location - requested location to compute distance to the center of the ring
  */
case class InputParams(location: Long)
