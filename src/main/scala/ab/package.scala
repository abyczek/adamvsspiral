package object ab {

  type Result[T] = Either[Wrong, T]

  sealed trait Wrong extends Product with Serializable

  final case class WrongWithMsg(errMsg: String) extends Wrong

  final case class WrongWithEx(errMsg: String, throwable: Throwable) extends Wrong

}
