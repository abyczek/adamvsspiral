package ab

object SpiralApp extends App with SpiralRunner {

  println("Fun with spirals")

  computeManhattanDistance(args) match {
    case Right(distance) =>
      println(s"Manhattan distance for the location: ${args(0)} is: $distance")
    case Left(WrongWithMsg(errorMsg)) =>
      println(s"Computation of the distance resulted with the error: $errorMsg")
    case Left(WrongWithEx(errorMsg, ex)) =>
      println(s"Computation of the distance reported problem: $errorMsg caused by the exception: ${ex.toString}")
  }
}






