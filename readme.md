# Project setup

This project has been tested for SBT version 1.3.8, however it does not employ advanced features,
hence it should work with most of SBT versions

Scala version is configured to be 1.12.4. Like SBT it should work with all 1.12.x versions.

Project has only two simple dependencies:

* scala test
* cats-core

It is assumed that those are easy to fetch by SBT

Application entry point is a class: ab.SpiralApp

Execution of the application:
$ sbt "run 1024"

![command line example](AdamVsSpiral_usage.jpg)


## NOTE
This solution has been implementd on IntellijIdea v 2018.1 but should be importable to most of available versions.


# Author's comments to the solution:

### Taken approach:
Given task seems to be possible to implement within one long-ish mathematical equation. However that wouldn't show scala coding really.
For that reason author decided to present solution to the task as it would be LOB application - that is introducing some abstractions at the cost of performance.

Main computations are based on Sequences. Those could be easily replaced with tail-recursive functions for example. 
However such an approach is more generic, where provided implementation could be easily translated to other streaming technologies like Reactive Extensions.

### Algorithm
Solution is based on 2 steps:

* finding a "ring" within the grid that holds given location (there is a feature that leverages solution that every next ring length is greater by 8 to the previous.)
* computing distance for that location to the center of the spiral.

### Possible improvements considered as not critical for this task:

This solution does not employ any logging frameworks - it relies on simple println.
The rationale for this is that business logic has been extracted as agnostic component. Furthermore, it tries to be pure without using IO monads (not using logging within).
It just returns one result either good or bad. Hosting component job would be to do something with it - here console app just prints to standard output.

Also hosting component (command line application) takes the simplest form by accepting input argument and trying to execute application.
It does not support retry mechanism for example. To execute another test case user has to run application again with different argument.

